local-phone-apps = $(private-phone-apps)
local-phone-priv-apps = $(private-phone-priv-apps)

private-phone-apps := BasicDreams \
		Bluetooth \
		BluetoothExt \
		CertInstaller \
		DocumentsUI \
		Galaxy4 \
		HoloSpiralWallpaper \
		HTMLViewer \
		KeyChain \
		LiveWallpapers \
		MagicSmokeWallpapers \
		NoiseField \
		Nfc \
		PacProcessor \
		PhaseBeam \
		PhotoTable \
		PrintSpooler \
		Stk \
		UserDictionaryProvider \
		VisualizationWallpapers \
		WAPPushManager

private-phone-priv-apps := BackupRestoreConfirmation \
		DefaultContainerService \
		InputDevices \
		ExternalStorageProvider \
		MediaProvider \
		ProxyHandler \
		SharedStorageBackup \
		Shell \
		Tag \
		WallpaperCropper
